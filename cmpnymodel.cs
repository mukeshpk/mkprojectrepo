﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace commonmodel
{
    public class cmpnymodel
    {
        public int id { get; set; }
        [Required(ErrorMessage ="required fields")]
        public string name { get; set; }
        [Required(ErrorMessage = "required fields")]
        public string address { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Phone Number Required!")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$",
                   ErrorMessage = "Entered phone format is not valid.")]
        public string mobnum { get; set; }
        public string gmail { get; set; }
        public int addressid { get; set; }
    }
}
